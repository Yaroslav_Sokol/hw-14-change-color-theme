function toggleTheme() {
    const body = document.querySelector('body');
    body.classList.toggle('light');
    body.classList.toggle('dark');
  }

const themeButton = document.querySelector('#theme-button');
themeButton.addEventListener('click', toggleTheme);

function toggleTheme() {
  const body = document.querySelector('body');
  body.classList.toggle('light');
  body.classList.toggle('dark');

  const currentTheme = body.classList.contains('light') ? 'light' : 'dark';
  localStorage.setItem('theme', currentTheme);
}

// При завантаженні сторінки перевірте, чи є збережена тема в локальному сховищі, і застосуйте її
const savedTheme = localStorage.getItem('theme');
if (savedTheme) {
  const body = document.querySelector('body');
  body.classList.add(savedTheme);
}